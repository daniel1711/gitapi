package com.livroapi;

import java.time.LocalDate;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.livroapi.modelo.Livro;
import com.livroapi.service.LivroService;

@SpringBootApplication
@ComponentScan(basePackages = "com.livroapi")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public CommandLineRunner loadData(LivroService service) {

		return (args) -> {
			service.salvar(new Livro(null, "Senhor das moscas", LocalDate.now(), "Disponível", "Vida",
					"http://statics.livrariacultura.net.br/products/capas_lg/829/15056829.jpg"));
			service.salvar(new Livro(null, "1984", LocalDate.now(), "Disponível", "Vida",
					"http://statics.livrariacultura.net.br/products/capas_lg/750/2823750.jpg"));			
			service.salvar(new Livro(null, "Revolução dos bichos", LocalDate.now(), "Disponível", "Vida",
					"http://statics.livrariacultura.net.br/products/capas_lg/877/1831877.jpg"));
			service.salvar(new Livro(null, "Admivavel mundo novo", LocalDate.now(), "Disponível", "Vida",
					"https://images.livrariasaraiva.com.br/imagemnet/imagem.aspx/?pro_id=7106246&qld=90&l=430&a=-1"));			
			service.salvar(new Livro(null, "Guerra dos mundos", LocalDate.now(), "Disponível", "Vida",
					"https://images.livrariasaraiva.com.br/imagemnet/imagem.aspx/?pro_id=9325414&qld=90&l=430&a=-1"));
			service.salvar(new Livro(null, "Cronicas de narnia", LocalDate.now(), "Disponível", "Vida",
					"https://images.livrariasaraiva.com.br/imagemnet/imagem.aspx/?pro_id=2623167&qld=90&l=430&a=-1"));
			service.salvar(new Livro(null, "Senhor dos Aneis", LocalDate.now(), "Disponível", "Vida",
					"https://images.livrariasaraiva.com.br/imagemnet/imagem.aspx/?pro_id=452744&qld=90&l=430&a=-1=1001479657"));
		};
	}

	@Bean
	public ModelMapper modelMapper() {
		ModelMapper mapper = new ModelMapper();
		mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		return mapper;
	}

}
