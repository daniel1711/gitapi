package com.livroapi.interfaces.dto;

import java.time.LocalDate;

import org.springframework.hateoas.ResourceSupport;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class LivroDto extends ResourceSupport{
	
	private String titulo;
	private LocalDate dataLancamento;
	private String situacao;
	private String editora;
	private String imagem;

}
