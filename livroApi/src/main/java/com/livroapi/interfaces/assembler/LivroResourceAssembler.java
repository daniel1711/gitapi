package com.livroapi.interfaces.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import com.livroapi.interfaces.dto.LivroDto;
import com.livroapi.modelo.Livro;
import com.livroapi.webapi.LivroController;

@Component
public class LivroResourceAssembler extends ResourceAssemblerSupport<Livro, LivroDto> {
	
	@Autowired
	private ModelMapper modelMapper;

	public LivroResourceAssembler() {
		super(LivroController.class, LivroDto.class);
	}

	@Override
	public LivroDto toResource(Livro entity) {
		LivroDto resource = createResourceWithId(entity.getId(), entity);
		modelMapper.map(entity, resource);
		return resource;
	}
	
	@Override
	public List<LivroDto> toResources(Iterable<? extends Livro> entities) {
		List<LivroDto> response = new ArrayList<>();
		for (Livro livro :entities) {
			LivroDto resource = toResource(livro);
			response.add(resource);
		}
		return response;
	}

}
