package com.livroapi.interfaces.facade;

import java.util.List;

import com.livroapi.interfaces.dto.LivroDto;

public interface LivroFacade {
	
	void salvar(LivroDto livro);

	void atualizar(LivroDto livro);

	void excluir(Long id);

	List<LivroDto> listar();

	LivroDto buscarPorId(Long id);

	LivroDto buscarPorNome(String nome);

}
