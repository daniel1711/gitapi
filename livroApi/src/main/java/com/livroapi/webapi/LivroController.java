package com.livroapi.webapi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.livroapi.interfaces.dto.LivroDto;
import com.livroapi.interfaces.facade.LivroFacade;

@RestController
@RequestMapping("/livro")
public class LivroController {
	
	@Autowired
	private LivroFacade facade;
	
	
	@GetMapping("/listar")
	public ResponseEntity<?> listar(){
		List<LivroDto> livro = facade.listar();
		return ResponseEntity.ok(livro);
	}

}
