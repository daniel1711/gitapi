package com.livroapi.service;

import java.util.List;
import java.util.Optional;

import com.livroapi.modelo.Livro;

public interface LivroService {

	void salvar(Livro livro);

	void atualizar(Livro livro);

	void excluir(Long id);

	List<Livro> listar();

	Optional<Livro> buscarPorId(Long id);

	Livro buscarPorNome(String nome);

}
