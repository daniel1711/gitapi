package com.livroapi.interfaces.facade.ipml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.livroapi.interfaces.assembler.LivroResourceAssembler;
import com.livroapi.interfaces.dto.LivroDto;
import com.livroapi.interfaces.facade.LivroFacade;
import com.livroapi.service.LivroService;

import core.estudo.erro.NotFoundException;



@Component
public class LivroFacadeIpml implements LivroFacade{
	
	@Autowired
	private LivroService service;
	
	@Autowired
	private LivroResourceAssembler assembler;

	@Override
	public void salvar(LivroDto livro) {
		//service.salvar(assembler.toResource(livro));
	}

	@Override
	public void atualizar(LivroDto livro) {
		//service.atualizar(livro);
	}

	@Override
	public void excluir(Long id) {
		//service.excluir(id);
	}

	@Override
	public List<LivroDto> listar() {
	
		List<LivroDto> resources = assembler.toResources(service.listar());
		if(resources.isEmpty()) throw new NotFoundException("Nenhum livro encontrado!");
		return resources;
	}

	@Override
	public LivroDto buscarPorId(Long id) {		
		//return assembler.toResource(service.buscarPorId(id));
		return null;
	}

	@Override
	public LivroDto buscarPorNome(String nome) {
		return assembler.toResource(service.buscarPorNome(nome));
	}

}
