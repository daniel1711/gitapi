package com.livroapi.service.ipml;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.livroapi.modelo.Livro;
import com.livroapi.repository.LivroRepository;
import com.livroapi.service.LivroService;

@Component
public class LivroServiceIpml implements LivroService{
	
	@Autowired
	private LivroRepository repository;

	@Override
	public void salvar(Livro livro) {
		repository.save(livro);
	}

	@Override
	public void atualizar(Livro livro) {
		repository.save(livro);
	}

	@Override
	public void excluir(Long id) {
		repository.deleteById(id);
	}

	@Override
	public List<Livro> listar() {
		return repository.findAll();
	}

	@Override
	public Optional<Livro> buscarPorId(Long id) {			
		return repository.findById(id);
	}

	@Override
	public Livro buscarPorNome(String nome) {		
		return null; // repository.findByName(nome);
	}

}
